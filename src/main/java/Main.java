/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * Exemplos de programação funcional
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * Adaptado de:
 * @author Speakjava (Simon Ritter)
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final String WORD_REGEXP = "[- .:,]+";
    private static Path pathSoneto;

    /**
     * Execução dos exemplos.
     */
    public static void main(String[] args) throws IOException {
        Main exemplos = new Main();
        exemplos.executa();
    }

    private Path getPathSoneto() {
        try {
            if (pathSoneto == null) {
                pathSoneto = Paths.get(ClassLoader.getSystemResource("soneto.txt").toURI());
            }
            return pathSoneto;
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public void executa() throws IOException {
        System.out.println("Exemplos de Lambdas e Streams");
        System.out.println("Executando Exemplo 1...");
        exemplo1();
        System.out.println("Executando Exemplo 2...");
        exemplo2();
        System.out.println("Executando Exemplo 3...");
        exemplo3();
        System.out.println("Executando Exemplo 4...");
        exemplo4();
        System.out.println("Executando Exemplo 5...");
        exemplo5();
        System.out.println("Executando Exemplo 6...");
        exemplo6();
        System.out.println("Executando Exemplo 7...");
        exemplo7();
    }

    /**
     * Exemplo 1
     *
     * Cria uma nova lista com todos os strings convertidos para minúsculo
     * e exibe o conteúdo da lista.
     */
    private void exemplo1() {
        List<String> list = Arrays.asList(
                "O", "Rato", "Roeu", "A", "Roupa", "Do", "Rei", "DE", "ROMA");

        List<String> novaList = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        novaList.forEach(System.out::println);
    }

    /**
     * Exemplo 2
     *
     * Modifica o exemplo anterior para que a lista resultante contenha apeanas
     * strings com comprimento ímpar.
     */
    private void exemplo2() {
        List<String> list = Arrays.asList(
                "O", "Rato", "Roeu", "A", "Roupa", "Do", "Rei", "DE", "ROMA");

        List<String> novaLista = list.stream()
                .filter(w -> w.length() % 2 == 1)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        novaLista.forEach(System.out::println);
    }

    /**
     * Exemplo 3
     *
     * Concatena os 4 primeiros strings da lista, separando cada palavra por
     * um hífen. Exibe o String resultante.
     */
    private void exemplo3() {
        List<String> list = Arrays.asList(
                "O", "Rato", "Roeu", "A", "Roupa", "Do", "Rei", "DE", "ROMA");

        String concat = list.stream()
                .skip(1)
                .limit(3)
                .collect(Collectors.joining("-"));
        System.out.println(concat);
    }

    /**
     * Exemplo 4
     *
     * Conta o número de linhas em um arquivo.
     */
    private void exemplo4() throws IOException {
        try (BufferedReader reader
                = Files.newBufferedReader(getPathSoneto())) {
            long n = reader.lines().count();
            System.out.println("Numero de linhas = " + n);
        }
    }

    /**
     * Exemplo 5
     *
     * Cria uma lista de palavras e um arquivo ignorando duplicatas. Exibe
     * as palavras encontradas.
     */
    private void exemplo5() throws IOException {
        try (BufferedReader reader
                = Files.newBufferedReader(getPathSoneto())) {

            List<String> palavras = reader.lines()
                    .flatMap(line -> Stream.of(line.split(WORD_REGEXP)))
                    .distinct()
                    .collect(Collectors.toList());
            palavras.stream()
                    .forEach(System.out::println);
        }
    }

    /**
     * Cria uma lista de palavras em um arquivo convertidas para minúsculo
     * ignorando duplicatas e ordenada. Exibe as palavras encontradas.
     * @throws IOException 
     */
    private void exemplo6() throws IOException {
        try (BufferedReader reader
                = Files.newBufferedReader(getPathSoneto())) {

            List<String> palavras = reader.lines()
                    .flatMap(line -> Stream.of(line.split(WORD_REGEXP)))
                    .map(String::toLowerCase)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());

            palavras.stream().forEach(System.out::println);
        }
    }

    /**
     * Modifica o exemplo anterior para ordenar pelo comprimento das palavras.
     * @throws IOException 
     */
    private void exemplo7() throws IOException {
        try (BufferedReader reader
                = Files.newBufferedReader(getPathSoneto())) {

            List<String> palavras = reader.lines()
                    .flatMap(line -> Stream.of(line.split(WORD_REGEXP)))
                    .map(String::toLowerCase)
                    .distinct()
                    .sorted((a, b) -> a.length() - b.length())
                    .collect(Collectors.toList());

            palavras.stream().forEach(System.out::println);
        }
    }

}
